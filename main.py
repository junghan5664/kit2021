from userdata import data           #유저 정보 임포트 (딕셔너리 자료형)
from enemydata import enemy         #적 전투기 정보 임포트 (리스트 자료형)
from function import func           #함수 임포트

print('\n게임 시작전, 로그인을 해주세요.')
id = input('아이디 : ').upper()
pw = input('비밀번호 : ').upper()
login = False

if id in data.keys():
    if data.get(id) == pw:
        print('\n로그인 성공! 게임화면으로 이동합니다.\n')
        print('갤러그 게임 시작!')
        while True:
            print('\n\n1.총알 발사 2.적 정보 확인 3.새로운 적 정보 입력 4.회피하기 (그 외 숫자 입력시 게임 종료)')
            number = input('커맨드를 입력하세요 : ')

            if int(number) == 1:
                print('\n앞에 위치한 전투기 목록 :', enemy)
                check1 = input('왼쪽부터 몇번째로 있는 전투기를 공격하실 건가요? : ')
                del enemy[int(check1)-1]
                print(check1,'번 전투기를 격추시켰다!')

            elif int(number) == 2:
                check2 = input('\n왼쪽부터 몇번째로 있는 전투기 정보를 확인하실건가요? : ')
                print('해당 전투기 정보는 :', enemy[int(check2)-1])
            
            elif int(number) == 3:
                check3 = input('\n새롭게 발견한 전투기 정보를 입력해주세요 : ')
                enemy.append(check3)
                print('\n전투기 정보 추가완료!')
                        
            elif int(number) == 4:
                func()
                        
            else: 
                print('\n게임을 종료합니다. 플레이해주셔서 감사합니다.')
                break

    else:
        print('\n비밀번호가 일치하지 않습니다.')
        print('프로그램을 종료합니다.')
else:
    print('\n아이디가 일치하지 않습니다.')
    print('프로그램을 종료합니다.')